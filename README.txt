The U.S. Postal Service shipping quote module determines cost for shipping an
order via USPS. It uses the USPS Web Services API to send the order details to
USPS and retrieve a result for display on the checkout page. This result will
be exactly the same as if you went to the USPS web site and filled in your
package details manually. The site administrator may restrict which USPS
shipping methods are presented to your customer for selection.

The module may be enabled like any other module from the Drupal administration
menu at  <em>Administer » Modules</em> (<em>admin/modules</em>). Once enabled,
the module is configured at <em>Administer » Store » Configuration » Shipping
quotes</em> (<em>admin/store/settings/quotes</em>).  Click the "configure" link
for the USPS shipping method to see the USPS administrative settings page.

To set up your site to use shipping quotes from the United States Postal
Service, the following steps must be taken:

* Make sure you have the latest version of Ubercart and the usps module.
* Enable the Ubercart "Shipping quotes" and "U.S. Postal Service" modules.
* You should have permissions to "configure quotes" for the "uc_quotes module".
* Create or login to your USPS.com account and sign up for a Web Tools User ID.
  You can find a link to register on the page at http://www.usps.com/webtools/ -
  look for "Register now to use USPS Web Tools".
* Once you have received your email with your Web Tools User ID in it, contact
  USPS personally and request access to the production server by email
  (icustomercare@usps.com) or phone (1-800-344-7779) because the requests
  Ubercart sends out will not work at all in testing or production mode if USPS
  does not grant you access personally. Tell them that you are using Ubercart
  which has been tested with their systems already.
* Enter the "Web Tools User ID" you received in the email from USPS.com into
  Ubercart's "USPS User ID" on the USPS quote method page at
  admin/store/settings/quotes/settings/usps.
* Make sure you have filled out your default store address in the shipping
  quote settings, as USPS will need the ZIP code from those settings.
* Enter the weight for all of your products in their product settings.
* Test: Attempt to get a shipping quote by adding things to your cart and
  checking out, fill out the forms and be sure to enter a shipping ZIP code.

At this point it should work, and you should get usable shipping quotes in the
pane during checkout. If not, enable the debugging information in the shipping
quote settings. Being able to see the raw requests and responses may be more
useful than the error messages that are returned.
