<?php

/**
 * @file
 * USPS administration menu items.
 */

/**
 * Configures USPS settings.
 *
 * @see usps_admin_settings_validate()
 * @see usps_admin_settings_submit()
 * @ingroup forms
 */
function usps_admin_settings($form, &$form_state) {

  // Put fieldsets into vertical tabs.
  $form['usps-settings'] = array(
    '#type' => 'vertical_tabs',
    '#attached' => array(
      'js' => array(
        'vertical-tabs' => drupal_get_path('module', 'usps') . '/usps.admin.js',
      ),
    ),
  );

  // Container for credential forms.
  $form['usps_credentials'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Credentials'),
    '#description' => t('Account number and authorization information.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#group'       => 'usps-settings',
  );

  $form['usps_credentials']['usps_user_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('USPS user ID'),
    '#description'   => t('To acquire or locate your user ID, refer to the <a href="!url">USPS documentation</a>.', array('!url' => 'http://drupal.org/node/1308256')),
    '#default_value' => variable_get('usps_user_id', ''),
  );

  $form['domestic'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('USPS Domestic'),
    '#description' => t('Set the conditions that will return a USPS quote.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#group'       => 'usps-settings',
  );

  $form['domestic']['usps_online_rates'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Display USPS "online" rates'),
    '#default_value' => variable_get('usps_online_rates', FALSE),
    '#description'   => t('Show your customer standard USPS rates (default) or discounted "online" rates.  Online rates apply only if you, the merchant, pay for and print out postage from the USPS <a href="https://sss-web.usps.com/cns/landing.do">Click-N-Ship</a> web site.'),
  );

  $form['domestic']['usps_env_services'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('USPS envelope services'),
    '#default_value' => variable_get('usps_env_services', array_keys(usps_envelope_services())),
    '#options'       => usps_envelope_services(),
    '#description'   => t('Select the USPS services that are available to customers. Be sure to include the services that the Postal Service agrees are available to you.'),
  );

  $form['domestic']['usps_services'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('USPS parcel services'),
    '#default_value' => variable_get('usps_services', array_keys(usps_services())),
    '#options'       => usps_services(),
    '#description'   => t('Select the USPS services that are available to customers. Be sure to include the services that the Postal Service agrees are available to you.'),
  );

  $form['international'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('USPS International'),
    '#description' => t('Set the conditions that will return a USPS International quote.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#group'       => 'usps-settings',
  );

  $form['international']['usps_intl_env_services'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('USPS international envelope services'),
    '#default_value' => variable_get('usps_intl_env_services', array_keys(usps_international_envelope_services())),
    '#options'       => usps_international_envelope_services(),
    '#description'   => t('Select the USPS services that are available to customers. Be sure to include the services that the Postal Service agrees are available to you.'),
  );

  $form['international']['usps_intl_services'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('USPS international parcel services'),
    '#default_value' => variable_get('usps_intl_services', array_keys(usps_international_services())),
    '#options'       => usps_international_services(),
    '#description'   => t('Select the USPS services that are available to customers. Be sure to include the services that the Postal Service agrees are available to you.'),
  );

  // Container for quote options.
  $form['usps_quote_options'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Quote options'),
    '#description'   => t('Preferences that affect computation of quote.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
    '#group'         => 'usps-settings',
  );

  $form['usps_quote_options']['usps_all_in_one'] = array(
    '#type'          => 'radios',
    '#title'         => t('Product packages'),
    '#default_value' => variable_get('usps_all_in_one', 1),
    '#options'       => array(
      0 => t('Each product in its own package'),
      1 => t('All products in one package'),
    ),
    '#description'   => t('Indicate whether each product is quoted as shipping separately or all in one package. Orders with one kind of product will still use the package quantity to determine the number of packages needed, however.'),
  );

  // Insurance.
  $form['usps_quote_options']['usps_insurance'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Package insurance'),
    '#default_value' => variable_get('usps_insurance', FALSE),
    '#description'   => t('When enabled, the quotes presented to the customer will include the cost of insurance for the full sales price of all products in the order.'),
    '#disabled'      => TRUE,
  );

  // Delivery Confirmation.
  $form['usps_quote_options']['usps_delivery_confirmation'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Delivery confirmation'),
    '#default_value' => variable_get('usps_delivery_confirmation', FALSE),
    '#description'   => t('When enabled, the quotes presented to the customer will include the cost of delivery confirmation for all packages in the order.'),
    '#disabled'      => TRUE,
  );

  // Signature Confirmation.
  $form['usps_quote_options']['usps_signature_confirmation'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Signature confirmation'),
    '#default_value' => variable_get('usps_signature_confirmation', FALSE),
    '#description'   => t('When enabled, the quotes presented to the customer will include the cost of signature confirmation for all packages in the order.'),
    '#disabled'      => TRUE,
  );

  // Container for markup forms.
  $form['usps_markups'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Markups'),
    '#description'   => t('Modifiers to the shipping weight and quoted rate.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
    '#group'         => 'usps-settings',
  );

  $form['usps_markups']['usps_rate_markup_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Rate markup type'),
    '#default_value' => variable_get('usps_rate_markup_type', 'percentage'),
    '#options'       => array(
      'percentage' => t('Percentage (%)'),
      'multiplier' => t('Multiplier (×)'),
      'currency'   => t('Addition (!currency)', array('!currency' => variable_get('uc_currency_sign', '$'))),
    ),
  );
  $form['usps_markups']['usps_rate_markup'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Shipping rate markup'),
    '#default_value' => variable_get('usps_rate_markup', '0'),
    '#description'   => t('Markup shipping rate quote by dollar amount, percentage, or multiplier.'),
  );

  // Form to select type of weight markup.
  $form['usps_markups']['usps_weight_markup_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Weight markup type'),
    '#default_value' => variable_get('usps_weight_markup_type', 'percentage'),
    '#options'       => array(
      'percentage' => t('Percentage (%)'),
      'multiplier' => t('Multiplier (×)'),
      'mass'       => t('Addition (!mass)', array('!mass' => '#')),
    ),
    '#disabled'      => TRUE,
  );

  // Form to select weight markup amount.
  $form['usps_markups']['usps_weight_markup'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Shipping weight markup'),
    '#default_value' => variable_get('usps_weight_markup', '0'),
    '#description'   => t('Markup shipping weight on a per-package basis before quote, by weight amount, percentage, or multiplier.'),
    '#disabled'      => TRUE,
  );

  // Container for label printing.
  $form['usps_labels'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Label Printing'),
    '#description'   => t('Preferences for USPS Delivery Confirmation label Printing. Additional permissions from USPS are required to use this feature.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
    '#group'         => 'usps-settings',
  );

  // Form to select label image format.
  $form['usps_labels']['usps_label_image_format'] = array(
    '#type'          => 'select',
    '#title'         => t('Select image format for shipping labels'),
    '#default_value' => variable_get('usps_label_image_format', 'PDF'),
    '#options'       => usps_label_image_types(),
  );

  // Form to select label image style.
  $form['usps_labels']['usps_label_style'] = array(
    '#type'          => 'radios',
    '#title'         => t('Select label style'),
    '#default_value' => variable_get('usps_label_style', 1),
    '#options'       => array(
      1 => theme('image', array(
             'path' => drupal_get_path('module', 'usps') . '/images/usps-label-1-small.png',
              'alt' => 'USPS Label option 1',
              'title' => '',
              'attributes' => array('class' => 'usps-label-image')
           )),
      2 => theme('image', array(
             'path' => drupal_get_path('module', 'usps') . '/images/usps-label-2-small.png',
              'alt' => 'USPS Label option 2',
              'title' => '',
              'attributes' => array('class' => 'usps-label-image')
           )),
    ),
  );

  // Form to select receipt page.
  $form['usps_labels']['usps_label_receipt_page'] = array(
    '#type'          => 'select',
    '#title'         => t('Number of pages'),
    '#default_value' => variable_get('usps_label_receipt_page', 'FALSE'),
    '#options'       => array(
      'FALSE' => t('Print label and receipt on same page.'),
      'TRUE'  => t('Print label and receipt on separate pages.'),
    ),
  );

  $period = drupal_map_assoc(array(86400, 302400, 604800, 1209600, 2419200, 0), 'format_interval');
  $period[0] = t('Forever');

  // Form to select how long labels stay on server.
  $form['usps_labels']['usps_label_lifetime'] = array(
    '#type'          => 'select',
    '#title'         => t('Label lifetime'),
    '#default_value' => variable_get('usps_label_lifetime', 0),
    '#options'       => $period,
    '#description'   => t('Controls how long labels are stored on the server before being automatically deleted. Cron must be enabled for automatic deletion. Default is never delete the labels, keep them forever.'),
  );

  // Taken from system_settings_form(). Only, don't use its submit handler.
  $form['actions']['#type']  = 'actions';
  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/store/settings/quotes'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }

  return $form;
}

/**
 * Validation handler for usps_admin_settings form.
 *
 * @see usps_admin_settings()
 * @see usps_admin_settings_submit()
 */
function usps_admin_settings_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['usps_rate_markup'])) {
    form_set_error('usps_rate_markup', t('Rate markup must be a numeric value.'));
  }
  if (!is_numeric($form_state['values']['usps_weight_markup'])) {
    form_set_error('usps_weight_markup', t('Weight markup must be a numeric value.'));
  }
}

/**
 * Submit handler for usps_admin_settings form.
 *
 * @see usps_admin_settings()
 * @see usps_admin_settings_validate()
 */
function usps_admin_settings_submit($form, &$form_state) {
  $fields = array(
    'usps_user_id',
    'usps_online_rates',
    'usps_env_services',
    'usps_services',
    'usps_intl_env_services',
    'usps_intl_services',
    'usps_rate_markup_type',
    'usps_rate_markup',
    'usps_weight_markup_type',
    'usps_weight_markup',
    'usps_all_in_one',
    'usps_insurance',
    'usps_delivery_confirmation',
    'usps_signature_confirmation',
  );

  foreach ($fields as $key) {
    $value = $form_state['values'][$key];

    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

  drupal_set_message(t('The configuration options have been saved.'));

  cache_clear_all();
  drupal_theme_rebuild();
}

/**
 * Convenience function to get USPS codes for label image format.
 *
 * @return
 *   An array of human-friendly names for available USPS label image formats.
 */
function usps_label_image_types() {
  return array(
    'TIF'   => t('TIF - Tagged Image File Format'),
    'PDF'   => t('PDF - Adobe Portable Document Format'),
    'NONE'  => t('No image returned - just delivery confirmation number.'),
  );
}
