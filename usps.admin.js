/**
 * @file
 * Utility functions to display settings summaries on vertical tabs.
 */

(function ($) {

Drupal.behaviors.uspsAdminFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-domestic', context).drupalSetSummary(function(context) {
      if ($('#edit-usps-online-rates').is(':checked')) {
        return Drupal.t('Using "online" rates');
      }
      else {
        return Drupal.t('Using standard rates');
      }
    });

    $('fieldset#edit-usps-markups', context).drupalSetSummary(function(context) {
      return Drupal.t('Rate markup') + ': '
        + $('#edit-usps-rate-markup', context).val() + ' '
        + $('#edit-usps-rate-markup-type', context).val() + '<br />'
        + Drupal.t('Weight markup') + ': '
        + $('#edit-usps-weight-markup', context).val() + ' '
        + $('#edit-usps-weight-markup-type', context).val();
    });

    $('fieldset#edit-usps-labels', context).drupalSetSummary(function(context) {
      var format = $('#edit-usps-label-image-format', context).val();
      return Drupal.t('@format output', { '@format': format });
    });
  }
};

})(jQuery);
