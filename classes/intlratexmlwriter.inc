<?php

/**
 * @file
 * USPSInternationalRateXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * USPS Rates Calculators API Reference, version 1.3, 17 April 2011.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 *
 */
class USPSInternationalRateXMLWriter extends USPSXMLWriter {

  /** Origin address for shipment. */
  protected $from;

  /** Destination address for shipment. */
  protected $to;

  /** Array of PackagingPackage objects. */
  protected $packages = array();


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for origin address.
   */
  public function setShipFromAddress(ShippingAddressInterface $from) {
    $this->from = $from;
    return $this;
  }

  /**
   * Mutator for destination address.
   */
  public function setShipToAddress(ShippingAddressInterface $to) {
    $this->to = $to;
    return $this;
  }

  /**
   * Mutator for packages property.
   */
  public function setPackages(array $packages) {
    $this->packages = $packages;
    return $this;
  }

  /**
   * Overrides USPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL();
  }

  /**
   * Writes USPS IntlRateV2Request XML into output buffer.
   *
   * @return
   *   The USPSXMLWriter object.
   */
  public function rateRequest() {
    module_load_include('inc', 'uc_usps', 'uc_usps.countries');
    // USPS does not use ISO 3166 country name in some cases so we
    // need to transform ISO country name into one USPS understands.
    //$shipto_country = uc_usps_country_map($this->to->getCountry());

    // Hardwire Canada until we can figure out what to do about country
    // information outside of Ubercart.
    $shipto_country = 'Canada';

    // The USPS Web Service REQUIRES that these XML elements be sent in
    // a specific order. Changing this order will cause the web service
    // to fail.  You have been warned ...

    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('IntlRateV2Request');
    $this->writeAttribute('USERID', $this->credentials->getUserID());
    $this->writeElement(  'Revision', '2');

    // Loop over Packages here
    // USPS only allows 25 packages per request, so we will eventually need
    // to check the number and handle >25 packages gracefully.
    $shipment_weight = 0;
    $package_id = 0;
    foreach ($this->packages as $package) {

      $this->startElement('Package');
      $this->writeAttribute('ID', $package_id++);
      $rate_type = variable_get('usps_online_rates', FALSE);

      $weight = $package->getShipWeight();
      $shipment_weight += $weight;

      $this->writeElement(  'Pounds', $pounds = intval($weight));
      $this->writeElement(  'Ounces', ceil(($weight - $pounds) * 16));
      $this->writeElement(  'Machinable', 'TRUE');
      $this->writeElement(  'MailType', ($rate_type ? 'ONLINE' : 'ALL'));
      $this->writeElement(  'ValueOfContents', $package->getPrice());
      $this->writeElement(  'Country', $shipto_country);

    $plist = usps_package_types();
    $pcode = array_rand($plist);

      $this->writeElement(  'Container', 'VARIABLE');
      $this->writeElement(  'Size', 'REGULAR');
      $this->writeElement(  'Width', '');
      $this->writeElement(  'Length', '');
      $this->writeElement(  'Height', '');
      $this->writeElement(  'Girth', '');
      $this->writeElement(  'OriginZip', $this->from->getPostalCode());
      //$this->writeElement(  'CommercialFlag');

      // Check if we need to add any special services to this package.
      if (variable_get('uc_usps_insurance', FALSE)) {
        $this->startElement('ExtraServices');
        $this->writeElement('ExtraService', '1');
        $this->endElement(); // ExtraServices
      }

      $this->endElement();// Package
    } // End loop over packages

    $this->endElement();//Shipment
    $this->endElement();//IntlRateV2Request
    $this->endDocument();

    return $this;
  }
}
