<?php

/**
 * @file
 * USPSConfirmXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * USPS Delivery Confirmation API User's Guide, version 8.1c, 22 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML delivery confirmation label request.
 *
 * @return
 *   DeliveryConfirmationV3.0Request XML document to send to USPS.
 */
class USPSConfirmXMLWriter extends USPSXMLWriter {

  /** Origin address for shipment. */
  protected $from;

  /** Destination address for shipment. */
  protected $to;

  /** Zip code of drop-off location. */
  protected $po_zip_code = '';


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for po_zip_code property.
   */
  public function setPOZipCode($po_zip_code) {
    $this->po_zip_code = $po_zip_code;
    return $this;
  }


  /**
   * Mutator for origin address.
   */
  public function setShipFromAddress(ShippingAddressInterface $from) {
    $this->from = $from;
    return $this;
  }

  /**
   * Mutator for destination address.
   */
  public function setShipToAddress(ShippingAddressInterface $to) {
    $this->to = $to;
    return $this;
  }

  /**
   * Overrides USPSXMLWriter::getURL().
   */
  public function getURL() {
    // Delivery Confirmation requires HTTPS.
    // USPS HTTPS server has a different base URL.
    return str_replace(
      array('http:',  'production', 'testing'),
      array('https:', 'secure',     'secure'),
      parent::getBaseURL()
    );
  }

  /**
   * Writes USPS DeliveryConfirmationV3.0Request XML into output buffer.
   *
   * @return
   *   The USPSXMLWriter object.
   */
  public function deliveryConfirmationRequest() {

    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('DeliveryConfirmationV3.0Request');
    //$this->startElement('DelivConfirmCertifyV3.0Request');
    $this->writeAttribute('USERID', $this->credentials->getUserID());
    $this->writeElement(  'Option', '1');
    $this->writeElement(  'ImageParameters');

    // Company name limited to 32 characters.
    $this->writeElement(    'FromName', substr($this->from->getName(), 0, 32));
    $this->writeElement(    'FromFirm', substr($this->from->getCompany(), 0, 32));
    $this->writeElement(  'FromAddress1', substr($this->from->getStreet1(), 0, 32));
    $this->writeElement(  'FromAddress2', substr($this->from->getStreet2(), 0, 32));
    $this->writeElement(  'FromCity', substr($this->from->getCity(), 0, 21));
    $this->writeElement(  'FromState', $this->from->getZone());
    $this->writeElement(  'FromZip5', $this->from->getPostalCode());
    $this->writeElement(  'FromZip4', '');

    $this->writeElement(    'ToName', substr($this->to->getName(), 0, 32));
    $this->writeElement(    'ToFirm', substr($this->to->getCompany(), 0, 32));
    $this->writeElement(  'ToAddress1', substr($this->to->getStreet1(), 0, 32));
    $this->writeElement(  'ToAddress2', substr($this->to->getStreet2(), 0, 32));
    $this->writeElement(  'ToCity', substr($this->to->getCity(), 0, 21));
    $this->writeElement(  'ToState', $this->to->getZone());
    $this->writeElement(  'ToZip5', $this->to->getPostalCode());
    $this->writeElement(  'ToZip4', '');

    $this->writeElement(  'WeightInOunces', '2');
    $this->writeElement(  'ServiceType', 'Priority');
    $this->writeElement(  'SeparateReceiptPage', 'FALSE');
    $this->writeElement(  'POZipCode', $this->po_zip_code);
    $this->writeElement(  'ImageType', 'PDF');
    $this->writeElement(  'LabelDate');

    //$this->writeElement(  'CustomerRefNo', 'any desired info');

    $this->endElement();// DeliveryConfirmationV3.0Request
    $this->endDocument();

    return $this;
  }

}
