<?php

/**
 * @file
 * USPSXMLWriter abstract base class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 *
 */
abstract class USPSXMLWriter extends XMLWriter {

  /** Shipper's USPS API credentials. */
  protected $credentials;

  /** Flag for testing service mode. */
  const TESTING    = 0;

  /** Flag for production service mode. */
  const PRODUCTION = 1;

  /** Base URL for testing API requests. */
  protected static $testing_base_url = 'http://testing.shippingapis.com/ShippingAPITest.dll';

  /** Base URL for production API requests. */
  protected static $production_base_url = 'http://production.shippingapis.com/ShippingAPI.dll';

  /** Default to PRODUCTION service mode. TESTING has limited capability. */
  protected $service_mode = USPSXMLWriter::PRODUCTION;

  /** Debug mode for output will pretty-print the XML. */
  protected $debug = FALSE;


  /**
   * Constructor.
   */
  public function __construct() {
    //parent::__construct();
    $this->openMemory();
  }

  /**
   * Mutator for credentials property.
   *
   * @param USPSCredentials $credentials
   *   An associative array containing the following keys:
   *   -accesslicensenumber:
   *   -userid:
   *   -password:
   */
  public function setCredentials(USPSCredentials $credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * Mutator for service_mode property.
   */
  public function setServiceMode($service_mode) {
// need to validate that this is either TESTING or PRODUCTION
    $this->service_mode= $service_mode;
    return $this;
  }

  /**
   * Accessor for base service URL.
   */
  protected function getBaseURL() {
    switch ($this->service_mode) {
      case USPSXMLWriter::PRODUCTION:
        return self::$production_base_url;
        break;

      case USPSXMLWriter::TESTING:
      default:
        return self::$testing_base_url;
        break;
    }
  }

  /**
   * Mutator for debug property.
   *
   * @param $debug
   *   TRUE to pretty-print the XML (line breaks and indentations).
   */
  public function setDebug($debug = FALSE) {
    $this->debug = $debug;
    $this->setIndent($debug);
    return $this;
  }

  /**
   * Accessor for debug property.
   */
  public function isDebug() {
    return $this->debug;
  }

  /**
   * Accessor for service URL.
   *
   * Subclasses must implement this. The typical implementation should
   * return parent::getBaseURL() . 'ServiceName', where 'ServiceName' is
   * a string containing the actual service name implemented by the subclass.
   */
  protected abstract function getURL();

  /**
   * Returns buffer as an XML string.
   */
  public function __toString() {
    return $this->outputMemory(TRUE);
  }
}
