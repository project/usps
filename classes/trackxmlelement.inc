<?php
/**
 * @file
 * Defines a SimpleXMLElement subclass specific to USPS Tracking responses.
 */

/**
 * Provides an interface to access XML containing USPS Tracking responses.
 */
class USPSTrackingXMLElement extends SimpleXMLElement implements ShippingTrackingInterface {

  /**
   * Implements ShippingTrackingInterface::getCarrier().
   */
  public function getCarrier() {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();
    return (string) 'U.S.P.S.';
  }

  /**
   * Implements ShippingTrackingInterface::getTrackingNumber().
   */
  public function getTrackingNumbers() {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    $tracking_numbers = array();
    foreach ($this->TrackInfo as $info) {
      $tracking_numbers[] = (string) $info['ID'];
    }
    return $tracking_numbers;
  }

  /**
   * Implements ShippingTrackingInterface::getStatus().
   */
  public function getStatus($tracking_number) {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    $info = $this->xpath("//TrackInfo[@ID='" . $tracking_number . "']");
    return $info[0]->TrackSummary->Event;
  }

  /**
   * Extracts package tracking information from response XML.
   *
   * @return
   *   An array of ShippingTrackingEvent objects.
   */
  public function getTrackingInfo($tracking_number) {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    $tracking_info = array();
    $info = $this->xpath("//TrackInfo[@ID='" . $tracking_number . "']");
    $info = $info[0];

    if (isset($info->Error)) {
      //This shouldn't be an Exception, as it's an expected and recoverable
      // situation.
      throw new ShippingTrackingException(t('USPS has no record of this item. Please verify your information and try again later. Error :error: - ":description".', array(':error' => $info->Error->Number, ':description' => $info->Error->Description)));
    }

    // Current status is stored in TrackSummary elements,
    // Intermediate status(es) are stored in TrackDetail elements.

    // Start a new event.
    $event = new ShippingTrackingEvent();
    $event->setTime((string) $info->TrackSummary->EventTime);
    $event->setDate((string) $info->TrackSummary->EventDate);
    $event->setEvent((string) $info->TrackSummary->Event);

    $location = '';
    if ($info->TrackSummary->EventCity != '') {
      $location = $info->TrackSummary->EventCity;
      if ($info->TrackSummary->EventState != '') {
        $location .= ', ' . $info->TrackSummary->EventState;
      }
    }
    else {
      $location = $info->TrackSummary->EventCountry;
    }
    $event->setLocation((string) $location);
    // Save event for return.
    $tracking_info[] = $event;

    // Now for all the previous details.
    foreach ($info->TrackDetail as $detail) {
      // Start a new event.
      $event = new ShippingTrackingEvent();
      $event->setTime((string) $detail->EventTime);
      $event->setDate((string) $detail->EventDate);
      $event->setEvent((string) $detail->Event);

      $location = '';
      if ($detail->EventCity != '') {
        $location = $detail->EventCity;
        if ($detail->EventState != '') {
          $location .= ', ' . $detail->EventState;
        }
      }
      else {
        $location = $detail->EventCountry;
      }
      $event->setLocation((string) $location);
      $tracking_info[] = $event;
    }
    return $tracking_info;

    // No tracking info found for this tracking number.
    throw new ShippingTrackingException(t('USPS response does not contain information about tracking number :number.', array(':number' => $tracking_number)));
  }

  /**
   * Checks XML Response for errors.
   */
  protected function checkForErrorResponse() {
    // Check for invalid or failed tracking request.
    if (strtolower($this->Name) == 'error') {
      throw new ShippingTrackingException(t('USPS has encountered an error processing your tracking request.'));
    }
  }

}
