<?php
/**
 * @file
 * Defines a SimpleXMLElement subclass specific to USPS Address responses.
 */

/**
 * Provides an interface to access XML containing USPS Address responses.
 */
class USPSAddressXMLElement extends SimpleXMLElement {
//class USPSAddressXMLElement extends SimpleXMLElement implements ShippingAddressInterface {

  /**
   * Implements ShippingAddressInterface::getCarrier().
   */
  public function getCarrier() {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();
    return (string) 'U.S.P.S.';
  }

  /**
   * Implements ShippingTrackingInterface::getTrackingNumber().
   */
  public function getAddressIDs() {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    $ids = array();
    foreach ($this->Address as $info) {
      $ids[] = (string) $info['ID'];
    }
    return $ids;
  }

  /**
   * Extracts package tracking information from response XML.
   *
   * @return
   *   An array of ShippingAddressEvent objects.
   */
  public function getAddress($id) {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    $info = $this->xpath("//Address[@ID='" . $id . "']");
    $info = $info[0];

    if (isset($info->Error)) {
      //This shouldn't be an Exception, as it's an expected and recoverable
      // situation.
      throw new ShippingAddressException(t('USPS has no record of this item. Please verify your information and try again later. Error :error: - ":description".', array(':error' => $info->Error->Number, ':description' => $info->Error->Description)));
    }

    // Create a new address object.
    $address = new ShippingAddress(array());
    $address->setCompany((string) $info->FirmName);
    $address->setStreet1((string) $info->Address1);
    $address->setStreet2((string) $info->Address2);
    $address->setCity((string) $info->City);
    $address->setZone((string) $info->State);
    $postal_code = (string) $info->Zip5;
    if (!empty($info->Zip4)) {
      $postal_code .= '-' . (string) $info->Zip4;
    }
    $address->setPostalCode($postal_code);

    return $address;
  }

  /**
   * Checks XML Response for errors.
   */
  protected function checkForErrorResponse() {
    // Check for invalid or failed tracking request.
    if (strtolower($this->Name) == 'error') {
      throw new ShippingAddressException(t('USPS has encountered an error processing your tracking request.'));
    }
  }

}
