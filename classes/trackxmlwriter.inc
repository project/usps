<?php

/**
 * @file
 * USPSTrackXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * USPS Track/Confirm API User's Guide, version 4.1a, 22 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML tracking request.
 *
 * @return
 *   TrackFieldRequest XML document to send to USPS.
 */
class USPSTrackXMLWriter extends USPSXMLWriter {

  /** Array of tracking numbers to look up. */
  protected $tracking_numbers = array();

  private $sequence = 0;


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for tracking_number property.
   *
   * Tracking requests are limited to a maximum of 10 tracking numbers.
   */
  public function setTrackingNumber($tracking_number, $sequence = NULL) {
    // Need to enforce <= 10 tracking numbers. We can actually send as many
    // as we like, but at most 10 results will be returned. Documentation
    // doesn't mention this, it was determined empirically.

    // Need to use a better data structure so we don't have to expose
    // sequence.
    if (isset($sequence)) {
      $this->tracking_numbers[$sequence] = $tracking_number;
    }
    else {
      $this->tracking_numbers[] = $tracking_number;
    }
    return $this;
  }

  /**
   * Overrides USPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL();
  }

  /**
   * Writes USPS TrackFieldRequest XML into output buffer.
   *
   * @return
   *   The USPSXMLWriter object.
   */
  public function trackRequest() {

    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('TrackFieldRequest');
    $this->writeAttribute('USERID', $this->credentials->getUserID());

    // Multiple TrackID fields are allowed, but at most 10 results will
    // be returned.
    foreach ($this->tracking_numbers as $tracking_number) {
      $this->startElement(  'TrackID');
      $this->writeAttribute('ID', $tracking_number);
      $this->endElement();// TrackID
    }

    $this->endElement();// TrackFieldRequest
    $this->endDocument();

    return $this;
  }

}
