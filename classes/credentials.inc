<?php

/**
 * @file
 * USPSCredentials utility class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Class for USPS credentials.
 */
class USPSCredentials {

  private $userid = '';

  /**
   * Constructor.
   *
   * @param array $credentials
   *   - userid:
   */
  public function __construct(array $credentials = array()) {
    // Defaults for unset keys.
    $credentials += array(
      'userid'              => '',
    );

    $this->userid              = $credentials['userid'];
  }

  /**
   * Mutator for userid property.
   */
  public function setUserID($userid) {
    $this->userid = $userid;
    return $this;
  }

  /**
   * Accessor for userid property.
   */
  public function getUserID() {
    return $this->userid;
  }
}
