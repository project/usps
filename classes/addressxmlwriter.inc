<?php

/**
 * @file
 * USPSAddressXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * USPS Address Information API USer's Guide, version 3.1b, 22 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML address standardization request.
 *
 * @return
 *   AddressValidateRequest XML document to send to USPS.
 */
class USPSAddressXMLWriter extends USPSXMLWriter {

  /** Array of addresses to validate */
  protected $addresses = array();

  private $sequence = 0;


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for address property.
   */
  public function setAddress(ShippingAddressInterface $address, $sequence = NULL) {
    // Need to enforce <= 5 elements in the address array.
    // Need to use a better data structure so we don't have to expose
    // sequence.
    if (isset($sequence)) {
      $this->addresses[$sequence] = $address;
    }
    else {
      $this->addresses[] = $address;
    }
    return $this;
  }

  /**
   * Overrides USPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL();
  }

  /**
   * Writes USPS AddressValidateRequest XML into output buffer.
   *
   * @return
   *   The USPSXMLWriter object.
   */
  public function addressValidateRequest() {
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('AddressValidateRequest');
    $this->writeAttribute('USERID', $this->credentials->getUserID());

    // USPS allows up to 5 Address tags per request.
    foreach ($this->addresses as $id => $address) {
      $this->startElement('Address');
      $this->writeAttribute('ID', $id);
      // Company name limited to 38 characters.
      //$this->writeElement(  'FirmName', substr($address->getCompany(), 0, 38));
      $this->writeElement(  'Address1', substr($address->getStreet1(), 0, 38));
      $this->writeElement(  'Address2', substr($address->getStreet2(), 0, 38));
      $this->writeElement(  'City', substr($address->getCity(), 0, 15));
      $this->writeElement(  'State', $address->getZone());
      $this->writeElement(  'Zip5', $address->getPostalCode());
      $this->writeElement(  'Zip4', '');
      $this->endElement();// Address
    }

    $this->endElement();// AddressValidateRequest
    $this->endDocument();

    return $this;
  }

}
