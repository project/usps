<?php

/**
 * @file
 * USPSRateXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * USPS Rates Calculators API Reference, version 1.3, 17 April 2011.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 *
 */
class USPSRateXMLWriter extends USPSXMLWriter {

  /** Origin address for shipment. */
  protected $from;

  /** Destination address for shipment. */
  protected $to;

  /** Array of PackagingPackage objects. */
  protected $packages = array();


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for origin address.
   */
  public function setShipFromAddress(ShippingAddressInterface $from) {
    $this->from = $from;
    return $this;
  }

  /**
   * Mutator for destination address.
   */
  public function setShipToAddress(ShippingAddressInterface $to) {
    $this->to = $to;
    return $this;
  }

  /**
   * Mutator for packages property.
   */
  public function setPackages(array $packages) {
    $this->packages = $packages;
    return $this;
  }

  /**
   * Overrides USPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL();
  }

  /**
   * Writes USPS RatingServiceSelectionRequest XML into output buffer.
   *
   * @return
   *   The USPSXMLWriter object.
   */
  public function rateRequest() {

    // The USPS Web Service REQUIRES that these XML elements be sent in
    // a specific order. Changing this order will cause the web service
    // to fail.  You have been warned ...

    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('RateV4Request');
    $this->writeAttribute('USERID', $this->credentials->getUserID());
    $this->writeElement(  'Revision', '2');

    // Loop over Packages here
    // USPS only allows 25 packages per request, so we will eventually need
    // to check the number and handle >25 packages gracefully.
    $shipment_weight = 0;
    $package_id = 0;
    foreach ($this->packages as $package) {

      $this->startElement('Package');
      $this->writeAttribute('ID', $package_id++);
      $rate_type = variable_get('usps_online_rates', FALSE);

      $this->writeElement(  'Service', ($rate_type ? 'ONLINE' : 'ALL'));

      $this->writeElement(  'ZipOrigination', $this->from->getPostalCode());
      $this->writeElement(  'ZipDestination', $this->to->getPostalCode());

      $weight = $package->getShipWeight();
      $shipment_weight += $weight;

      $this->writeElement(  'Pounds', $pounds = intval($weight));
      $this->writeElement(  'Ounces', ceil(($weight - $pounds) * 16));

    $plist = usps_package_types();
    $pcode = array_rand($plist);

      $this->writeElement(  'Container', 'VARIABLE');
      // If any dimension is > 12 inches the Size is considered LARGE.
      //if ($package->getWidth()  > 12 ||
      //    $package->getLength() > 12 ||
      //    $package->getHeight() > 12   ) {
      //  $this->writeElement(  'Size', 'LARGE');
        // Width, Length, and Height, and Girth are required for LARGE packages.
        //$this->writeElement(  'Width', '');
        //$this->writeElement(  'Length', '');
        //$this->writeElement(  'Height', '');
        // Is this the right way to calculate Girth?
        //$girth = 2 * $package->width + 2 * $package->height;
        //$this->writeElement(  'Girth', '');
      //}
      //else {
        $this->writeElement(  'Size', 'REGULAR');
      //}

      //$package->machinable = (
      //  $package->length >= 6 && $package->length <= 34 &&
      //  $package->width >= 0.25 && $package->width <= 17 &&
      //  $package->height >= 3.5 && $package->height <= 17 &&
      //  ($package->pounds == 0 ? $package->ounces >= 6 : TRUE) &&
      //  $package->pounds <= 35 &&
      //  ($package->pounds == 35 ? $package->ounces == 0 : TRUE)
      //);

      $this->writeElement(  'Value', $package->getPrice());

      // Check if we need to add any special services to this package.
      if (variable_get('usps_insurance', FALSE)             ||
          variable_get('usps_delivery_confirmation', FALSE) ||
          variable_get('usps_signature_confirmation', FALSE)  ) {

        $this->startElement('SpecialServices');

        if (variable_get('usps_insurance', FALSE)) {
          $this->writeElement('SpecialService', '1');
        }
        if (variable_get('usps_delivery_confirmation', FALSE)) {
          $this->writeElement('SpecialService', '13');
        }
        if (variable_get('usps_signature_confirmation', FALSE)) {
          $this->writeElement('SpecialService', '15');
        }

        $this->endElement(); // SpecialServices
      }

      $this->writeElement(  'Machinable', 'TRUE');
      $this->writeElement(  'ReturnLocations', 'TRUE');
      $this->writeElement(  'ShipDate', format_date(REQUEST_TIME, 'custom', 'd-M-Y', 'America/New_York'));
      $this->writeAttribute('Option', 'EMSH');


      $this->endElement();// Package
    } // End loop over packages

    $this->endElement();//Shipment
    $this->endElement();//RateV4Request
    $this->endDocument();

    return $this;
  }
}
